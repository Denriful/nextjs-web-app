
tee $SERVICE <<-EOL
apiVersion: v1
kind: Service
metadata:
  name: ${IMAGE_NAME}
spec:
  selector:
    app: ${IMAGE_NAME}
  ports:
    - protocol: TCP
      port: 3000
      targetPort: 3000

EOL

