
tee $DEPLOYMENT <<-EOL
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: ${IMAGE_NAME}
  name: ${IMAGE_NAME}
  namespace: ${NAMESPACE}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: ${IMAGE_NAME}
  strategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: ${IMAGE_NAME}
    spec:
      containers:
        - name: ${IMAGE_NAME}
          image: ${IMAGE_TAG}
          imagePullPolicy: IfNotPresent
          livenessProbe:
            failureThreshold: 3
            httpGet:
              path: /
              port: 3000
              scheme: HTTP
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 10
            initialDelaySeconds: 60
          readinessProbe:
            failureThreshold: 3
            httpGet:
              path: /
              port: 3000
              scheme: HTTP
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 10
            initialDelaySeconds: 30
          ports:
            - containerPort: 3000
              protocol: TCP
          resources:
            requests:
              cpu: ${CPU}
              memory: ${MEMORY}

EOL

