## PS - HowTo run localy

3. docker build -t nextjs-web-app:1.0.1 .
4. docker run -p 3000:3000 nextjs-web-app:1.0.1  

## PS - HowTo run CI/CD on GKE
1. Clone repo to your Gitlab project.
2. You need gitlab-runner type - docker, to run this pipeline
3. Create $GCP_KEYFILE variable in your CI/CD settings - google cloud service account token with access to GKE API.
4. Set variables in gitlab-ci.yml:
    - $GITLAB_RUNNER_TAG
    - $IMAGE_TAG
    - $PROJECT
    - $CLUSTER
    - $ZONE
    - $NAMESPACE
